
import { Door1 } from "./Door1";
import { Door2 } from "./Door2";
import { SquareButton } from "./SquareButton";
import { Text } from "./Text";

import { movePlayerTo } from '@decentraland/RestrictedActions'
import utils from '../node_modules/decentraland-ecs-utils/index'
import { MovableEntity } from "./MovableEntity";
import { SpaceTrigger } from "./SpaceTrigger";
import { RotatableEntity } from "./RotatableEnttiy";
import { Model } from "./Model";
import { Wawawa } from "./ui/wawawa";
import * as EthereumController from "@decentraland/EthereumController"
import * as crypto from "@dcl/crypto-scene-utils"

const gameCanvas = new UICanvas();

// Just a door
const door = new Door1();
door.addComponent(new Transform({
  position: new Vector3(2, 0, 2),
}))
door.addComponent(new OnClick(() => { door.open() }));

// Button to play sound
const btn = new SquareButton();
btn.addComponent(new Transform({
  position: new Vector3(4, 1, 4),
}));
btn.addComponent(new AudioSource(new AudioClip("sounds/movie_intro.mp3")));
btn.addComponent(new OnClick(() => {
  btn.getComponent(Animator).getClip("Button_Action").play();
  btn.getComponent(AudioSource).playOnce();
}));

// Text: Welcome to Metaverse!
const text = new Text("");
text.addComponent(new Transform({
  position: new Vector3(6, 1, 8),
  rotation: Quaternion.Euler(0, 90, 0),
}));

const colors = [Color3.Red(), Color3.Green(), Color3.Blue(), Color3.Yellow()];
let p = 0;
let welcomeText = "Welcome To Metaverse!";
let t = 0;
text.addComponent(new utils.Interval(200, () => {
  text.getComponent(TextShape).color = colors[p];
  p++
  if (p >= colors.length) {
    p = 0;
  }
  text.getComponent(TextShape).value = welcomeText.substr(0, t);
  t++;
  if (t >= welcomeText.length + 20) {
    t = 0;
  }
}));

// Counter making sounds
const formatTimeString = (seconds: number): string => {
  const minutes = Math.floor(seconds / 60);
  const secondsLeft = seconds % 60;
  return `${minutes}:${secondsLeft < 10 ? "0" : ""}${secondsLeft}`;
}
let counterText = new Text("0:00");
counterText.addComponent(new Transform({
  position: new Vector3(8, 1, 8),
}));
counterText.getComponent(TextShape).fontSize = 3;
counterText.getComponent(TextShape).color = Color3.Green();
counterText.addComponent(new AudioSource(new AudioClip("sounds/counter.mp3")));
let secondCount = 0;
counterText.addComponent(new utils.Interval(1000, () => {
  secondCount++;
  if (secondCount % 10 === 0) {
    counterText.getComponent(AudioSource).playOnce();
  }
  counterText.getComponent(TextShape).value = formatTimeString(secondCount);
}));

// Naughty Door
const naughtyDoor = new MovableEntity(
  new GLTFShape("models/Door1.glb"),
  { position: new Vector3(2, 0, 13) },
  new AudioClip("sounds/ohno.mp3"),
  new Vector3(10, 0, 0),
)
naughtyDoor.addComponent(new OnClick(() => {
  naughtyDoor.getComponent(utils.ToggleComponent).toggle();
}))

// Door not touchable
const door2 = new MovableEntity(
  new GLTFShape("models/Door1.glb"),
  { position: new Vector3(10, 0, 4) },
  new AudioClip("sounds/ohno.mp3"),
  new Vector3(0, 5, 0),
)
const trigger = new SpaceTrigger(
  new Vector3(8, 4, 8),
  new Vector3(8, 0, 0),
  () => {
    door2.getComponent(utils.ToggleComponent).toggle();
  },
  () => {
    door2.getComponent(utils.ToggleComponent).toggle();
  }
)

// Rotate Thing
const chandelier = new RotatableEntity(
  new GLTFShape("models/Chandelier.glb"),
  { position: new Vector3(8, 0, 8) },
  new AudioClip("sounds/counter.mp3"),
  Quaternion.Euler(0, 180, 0),
)
chandelier.addComponent(new OnClick(() => {
  chandelier.getComponent(utils.ToggleComponent).toggle();
}))

// Popup Image
const picture = new Model(
  new GLTFShape("models/Picture.glb"),
  { position: new Vector3(10, 1, 10) },
)
const image = new Wawawa(gameCanvas, new Texture("images/wawawa.png"));
image.container.visible = false;
picture.addComponent(new OnClick(() => {
  image.container.visible = true;
}))

// Restricted Door
const restrictedDoor = new Door2();
restrictedDoor.addComponent(new Transform({
  position: new Vector3(10, 0, 14),
  rotation: Quaternion.Euler(0, 270, 0),
}));
restrictedDoor.addComponent(new OnClick(async () => {
  const ok = await checkTokens();
  if (ok) {
    restrictedDoor.open();
  }
}));

// Restricted Area
const restrictedArea = new SpaceTrigger(
  new Vector3(8, 4, 8),
  new Vector3(14, 0, 14),
  async () => {
    const ok = await checkTokens();
    if (!ok) {
      movePlayerTo({ x: 1, y: 0, z: 1 }, { x: 8, y: 1, z: 8 })
    }
  },
  () => { }
)

const contractAddress = "0x9717e477cc5869a4a228361492b9bf7b8db58582"

let userAddress: string
executeTask(async () => {
  try {
    userAddress = await EthereumController.getUserAccount()
    log("User Address: ", userAddress)
  } catch (error) {
    log(error)
  }
})

async function checkTokens(): Promise<boolean> {
  let balance = await crypto.currency.balance(contractAddress, userAddress)
  log("BALANCE: ", balance)

  if (Number(balance) > 0) {
    return true
  }
  return false;
}

// Video
// #1
const myVideoClip = new VideoClip(
  "images/dao_promotion.mp4",
)
const streamClip = new VideoClip(
  "https://multiplatform-f.akamaihd.net/i/multi/will/bunny/big_buck_bunny_,640x360_400,640x360_700,640x360_1000,950x540_1500,.f4v.csmil/master.m3u8"
)

// #2
const myVideoTexture = new VideoTexture(myVideoClip)
const streamTextrue = new VideoTexture(streamClip)
let isStream = false

// #3
const myMaterial = new Material()
myMaterial.albedoTexture = myVideoTexture
myMaterial.roughness = 1
myMaterial.specularIntensity = 0
myMaterial.metallic = 0


// #4
const screen = new Entity()
screen.addComponent(new PlaneShape())
screen.addComponent(
  new Transform({
    position: new Vector3(14, 4, 10),
    rotation: Quaternion.Euler(0, 270, 0),
    scale: new Vector3(10 * 1, 5.6 * 1, 0.01),
  })
)
screen.addComponent(myMaterial)
screen.addComponent(
  new OnPointerDown(() => {
    isStream = !isStream
    if (isStream) {
      myVideoTexture.pause()
      screen.getComponent(Material).albedoTexture = streamTextrue
      streamTextrue.play()
    } else {
      streamTextrue.pause()
      screen.getComponent(Material).albedoTexture = myVideoTexture
      myVideoTexture.play()
    }
  })
)
engine.addEntity(screen)

// #5
myVideoTexture.play()