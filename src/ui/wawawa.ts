
export class Wawawa {
    public container: UIContainerRect;

    constructor(gameCanvas: UICanvas, texture: Texture) {
        this.container = new UIContainerRect(gameCanvas);
        this.container.width = "100%";
        this.container.height = "100%";

        const image = new UIImage(this.container, texture);
        image.sourceWidth = 600;
        image.sourceHeight = 600;
        image.width = 512;
        image.height = 512;

        const close = new UIImage(this.container, new Texture("images/button_close.png"));
        close.sourceHeight = 92
        close.sourceWidth = 92
        close.width = 46
        close.height = 46
        close.positionX = 256
        close.positionY = 256

        close.onClick = new OnClick((): void => {
            this.container.visible = false;
        })
    }
}