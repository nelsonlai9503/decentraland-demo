
export class Text extends Entity {
    constructor(text: string) {
        super();
        this.addComponent(new TextShape(text));
        engine.addEntity(this);
    }
}