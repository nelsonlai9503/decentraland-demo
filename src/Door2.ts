
export class Door2 extends Entity {

    isOpen: boolean;

    constructor() {
        super();
        this.isOpen = false;
        this.addComponent(new GLTFShape("models/Door2.glb"));
        this.addComponent(new Animator());
        this.getComponent(Animator).addClip(new AnimationState("OpenDoor", { looping: false }));
        this.getComponent(Animator).addClip(new AnimationState("CloseDoor", { looping: false }));
        this.addComponent(new AudioSource(new AudioClip("sounds/door_squeak.mp3")));
        engine.addEntity(this);
    }

    public open() {
        if (!this.isOpen) {
            this.getComponent(Animator).getClip("OpenDoor").play();
            this.getComponent(AudioSource).playOnce();
            this.isOpen = true;
        } else {
            this.getComponent(Animator).getClip("CloseDoor").play();
            this.getComponent(AudioSource).playOnce();
            this.isOpen = false;
        }
    }

}