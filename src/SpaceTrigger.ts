
import utils from '../node_modules/decentraland-ecs-utils/index'

export class SpaceTrigger extends Entity {

    constructor(
        boxSize: Vector3,
        boxPosition: Vector3,
        onCameraEnter?: () => void,
        onCameraExit?: () => void
    ) {
        super();
        engine.addEntity(this);
        this.addComponent(new utils.TriggerComponent(
            new utils.TriggerBoxShape(boxSize, boxPosition),
            {
                layer: 0,
                triggeredByLayer: 0,
                onCameraEnter: onCameraEnter,
                onCameraExit: onCameraExit,
            }
        ))
    }
}
