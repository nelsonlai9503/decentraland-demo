
export class SquareButton extends Entity {

    constructor() {
        super();
        this.addComponent(new GLTFShape("models/Square_Button.glb"));
        this.addComponent(new Animator());
        this.getComponent(Animator).addClip(new AnimationState("Button_Action", { looping: false }));
        engine.addEntity(this);
    }
}