
export class Door1 extends Entity {

    isOpen: boolean;

    constructor() {
        super();
        this.isOpen = false;
        this.addComponent(new GLTFShape("models/Door1.glb"));
        this.addComponent(new Animator());
        this.getComponent(Animator).addClip(new AnimationState("Door_Open", { looping: false }));
        this.getComponent(Animator).addClip(new AnimationState("Door_Close", { looping: false }));
        this.addComponent(new AudioSource(new AudioClip("sounds/door_squeak.mp3")));
        engine.addEntity(this);
    }

    public open() {
        if (!this.isOpen) {
            this.getComponent(Animator).getClip("Door_Open").play();
            this.getComponent(AudioSource).playOnce();
            this.isOpen = true;
        } else {
            this.getComponent(Animator).getClip("Door_Close").play();
            this.getComponent(AudioSource).playOnce();
            this.isOpen = false;
        }
    }

}